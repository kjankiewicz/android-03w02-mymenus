Tytuł: **Wykorzystanie różnych rodzajów menu oraz akcji kontekstowych**

Zaprezentowane zagadnienia:

* Różne rodzaje menu: menu opcji, menu kontekstowe, popup
* Wykorzystanie akcji kontekstowych
* Układy rozkładów
* Obsługa paska akcji
* Uruchamianie aktywności z wykorzystaniem jawnych intencji
* Przekazywanie danych za pośrednictwem intencji

Projekt prezentuje wykorzystanie różnych rodzajów menu oraz akcji kontekstowych do nawigacji pomiędzy aktywnościami. Sama nawigacja jest realizowana z użyciem jawnych intencji z przekazaniem dodatkowej informacji wenątrz intencji o klasie aktywności rozpoczynającej nawigację. Aktywności wykorzystane w aplikacji korzystają w swoich plikach rozkładu z różnych elementów rozkładu m. in. *LinearLayout*, *RelativeLayout*.

Najważniejsze pliki:

* *MainActivity.java* - główna aktywność aplikacji zawiera obsługę różnych rodzajów menu: menu opcji, menu kontekstowego oraz zarządza paskiem akcji przez wyłączenie paska tytułu i przycisku * Home
* *SecondActivity.java* - aktywność zawierająca obsługę menu opcji oraz akcji kontekstowych
* *ThirdActivity.java* - aktywność zawierająca obsługę menu opcji oraz menu popup
* *activity_main.xml* - plik układu głównej aktywności przedstawia wykorzystanie rozkładu *RelativeLayout*, do wyświetlenia pola tekstowego
* *activity_second.xml* - plik układu aktywności *SecondActivity* przedstawia wykorzystanie rozkładu *LinearLayout*, do wyświetlenia pola tekstowego
* *activity_third.xml* - plik układu aktywności *ThirdActivity* przedstawia wykorzystanie rozkładu *RelativeLayout*, do wyświetlenia pola tekstowego i przycisku z grafiką