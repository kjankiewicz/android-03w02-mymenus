package com.example.kjankiewicz.android_03w02

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.PopupMenu

class ThirdActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_third)
        val myToolbar: Toolbar = findViewById(R.id.my_toolbar)
        setSupportActionBar(myToolbar)
        val actionBar = this.supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
    }

    /*
     * Utworzenie menu opcji
     * Android-03w-interfejs-cz1: slajd 27
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.third, menu)
        return true
    }

    /*
     * Obsługa menu opcji
     * Android-03w-interfejs-cz1: slajd 27
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId
        if (id == R.id.open_main_activity) {
            /*
             * Użycie jawnej intencji do uruchomienia aktywności
             * Android-02w-aplikacje: slajd 31
             */
            val intent = Intent(applicationContext, MainActivity::class.java)
            intent.putExtra("ParentClassName", this.localClassName)
            startActivity(intent)
            return true
        }
        if (id == R.id.open_second_activity) {
            val intent = Intent(applicationContext, SecondActivity::class.java)
            intent.putExtra("ParentClassName", this.localClassName)
            startActivity(intent)
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun getParentActivityIntent(): Intent? {
        val parentIntent = intent
        val className = parentIntent.getStringExtra(
                "ParentClassName")
        val pckName = this.packageName

        var newIntent: Intent? = null
        try {
            newIntent = Intent(
                    this,
                    Class.forName(
                            "$pckName.$className"))
        } catch (e: ClassNotFoundException) {
            e.printStackTrace()
        }
        return newIntent
    }

    /*
     * Obsługa menu popup
     * Android-03w-interfejs-cz1: slajdy 31, 32
     */
    fun showPopup(v: View) {
        val popup = PopupMenu(this, v)
        popup.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.open_main_activity -> {
                    val intent = Intent(applicationContext,
                            MainActivity::class.java)
                    startActivity(intent)
                    true
                }
                R.id.open_second_activity -> {
                    val intent = Intent(applicationContext,
                            SecondActivity::class.java)
                    startActivity(intent)
                    true
                }
                else ->
                false
            }
        }
        val inflater = popup.menuInflater
        inflater.inflate(R.menu.third_popup_menu, popup.menu)
        popup.show()
    }

}
