package com.example.kjankiewicz.android_03w02

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.support.v7.view.ActionMode
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView

class SecondActivity : AppCompatActivity() {

    internal var mActionMode: ActionMode? = null

    /*
     * Odbiornik do obsługa akcji kontekstowych
     * Android-03w-interfejs-cz1: slajdy 29, 30
     */
    private val mActionModeCallback = object : ActionMode.Callback {

        override fun onCreateActionMode(mode: ActionMode,
                                        menu: Menu): Boolean {
            val inflater = mode.menuInflater
            inflater.inflate(R.menu.second_action_mode, menu)
            return true
        }

        override fun onPrepareActionMode(mode: ActionMode,
                                         menu: Menu): Boolean {
            return false
        }

        override fun onActionItemClicked(mode: ActionMode,
                                         item: MenuItem): Boolean {
            when (item.itemId) {
                R.id.open_main_activity -> {
                    val intent = Intent(applicationContext,
                            MainActivity::class.java)
                    startActivity(intent)
                    mode.finish()
                    return true
                }
                R.id.open_third_activity -> {
                    val intent = Intent(applicationContext,
                            ThirdActivity::class.java)
                    startActivity(intent)
                    mode.finish()
                    return true
                }
                else -> return false
            }
        }

        override fun onDestroyActionMode(mode: ActionMode) {
            mActionMode = null
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        val myToolbar = findViewById<Toolbar>(R.id.my_toolbar)
        setSupportActionBar(myToolbar)

        val actionBar = this.supportActionBar
        actionBar?.setDisplayHomeAsUpEnabled(true)
        val tv: TextView = findViewById(R.id.text_activity_second)
        tv.setOnLongClickListener {
            when (mActionMode) {
                null -> {
                    mActionMode = startSupportActionMode(mActionModeCallback)
                    it.isSelected = true
                    true
                }
                else -> false
            }
        }
    }

    /*
     * Utworzenie menu opcji
     * Android-03w-interfejs-cz1: slajd 27
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.second, menu)
        return true
    }

    /*
     * Obsługa menu opcji
     * Android-03w-interfejs-cz1: slajd 27
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId
        if (id == R.id.open_main_activity) {
            /*
             * Użycie jawnej intencji do uruchomienia aktywności
             * Android-02w-aplikacje: slajd 31
             */
            val intent = Intent(applicationContext, MainActivity::class.java)
            intent.putExtra("ParentClassName", this.localClassName)
            startActivity(intent)
            return true
        }
        if (id == R.id.open_third_activity) {
            val intent = Intent(applicationContext, ThirdActivity::class.java)
            intent.putExtra("ParentClassName", this.localClassName)
            startActivity(intent)
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun getParentActivityIntent(): Intent? {
        val parentIntent = intent
        val className = parentIntent.getStringExtra("ParentClassName")
        val pckName = this.packageName

        var newIntent: Intent? = null
        try {
            newIntent = Intent(
                    this,
                    Class.forName("$pckName.$className"))
        } catch (e: ClassNotFoundException) {
            e.printStackTrace()
        }
        return newIntent
    }
}
