package com.example.kjankiewicz.android_03w02

import android.content.Intent
import android.os.Bundle
import android.view.ContextMenu
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView

import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val myToolbar = findViewById<Toolbar>(R.id.my_toolbar)
        setSupportActionBar(myToolbar)

        val actionBar = this.supportActionBar
        if (actionBar != null) {

            /** Wyłączenie paska tytułu i przycisku Home w pasku akcji
             * Android-03w-interfejs-cz1: slajdy 34 */

            /* actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setDisplayShowTitleEnabled(false);*/
        }
        val tv: TextView = findViewById(R.id.text_activity_main)
        registerForContextMenu(tv)
    }

    /*
     * Utworzenie menu opcji
     * Android-03w-interfejs-cz1: slajd 27
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main, menu)
        return true
    }

    /*
     * Obsługa menu opcji
     * Android-03w-interfejs-cz1: slajd 27
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.open_second_activity -> {
                val intent = Intent(applicationContext, SecondActivity::class.java)
                intent.putExtra("ParentClassName", this.localClassName)
                startActivity(intent)
                return true
            }
            R.id.open_third_activity -> {
                val intent = Intent(applicationContext, ThirdActivity::class.java)
                intent.putExtra("ParentClassName", this.localClassName)
                startActivity(intent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    /*
     * Utworzenie menu kontekstowego
     * Android-03w-interfejs-cz1: slajd 28
     */
    override fun onCreateContextMenu(menu: ContextMenu, v: View,
                                     menuInfo: ContextMenu.ContextMenuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo)
        val inflater = menuInflater
        inflater.inflate(R.menu.main, menu)
    }

    /*
     * Obsługa menu kontekstowego
     * Android-03w-interfejs-cz1: slajd 28
     */
    override fun onContextItemSelected(item: MenuItem): Boolean {
        val intent: Intent
        when (item.itemId) {
            R.id.open_second_activity -> {
                /*
                 * Użycie jawnej intencji do uruchomienia aktywności
                 * Android-02w-aplikacje: slajd 31
                 */
                intent = Intent(applicationContext, SecondActivity::class.java)
                startActivity(intent)
                return true
            }
            R.id.open_third_activity -> {
                intent = Intent(applicationContext, ThirdActivity::class.java)
                startActivity(intent)
                return true
            }
            else -> return super.onContextItemSelected(item)
        }
    }
}
